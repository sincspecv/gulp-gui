/**
 * Created by mschroeter on 7/13/17.
 */
'use strict';

const electron = require('electron');
const {app, BrowserWindow} = require('electron');
const ipcMain = electron.ipcMain;
const {exec, spawn} = require('child_process');
const displayNotification = require('display-notification');
const getGulpTasks = require('get-gulp-tasks');
const _ = require('lodash');
const fkill = require('fkill');
const Store = require('electron-store');
const fixPath = require('fix-path');
const process = require('find-process');

let mainWindow = null;
let taskPID = null;
const store = new Store;

app.on('ready', () => {
    mainWindow = new BrowserWindow({
        height: 600,
        width:800,
        icon:  `${__dirname}/media/icon.icns`
    });

// console.log(app.getPath("userData"));

    //Disable default menu
    mainWindow.setMenu(null);

    // mainWindow.webContents.openDevTools();

    mainWindow.on('closed', function() {

        if(taskPID !== null) {
            fkill('gulp').catch(err => {
                console.log(err);
            });
        }

        mainWindow = null;
    });

    mainWindow.webContents.on('crashed', () => {
        console.log('crashed');
        fkill('gulp').catch(err => {
            console.error(err);
        });
        mainWindow = null;
    });

    //Load index.html into main window
    mainWindow.loadURL(`file://${__dirname}/app/index.html`);

    //Fix $PATH variable on macOS
    fixPath();

});

app.on('window-all-closed', () => {
    app.quit();
});

//Get new gulp task after drag/drop
ipcMain.on('get-new-gulp-tasks', (event, arg) => {
    getGulpTasks(arg)
        .then(tasks => {
        event.sender.send('add-new-gulp-tasks', tasks);
      });
});

//Check for process
ipcMain.on('check-for-process', (event, arg) => {

})

//Save new project to json store
exports.saveNewProject = (projObj) => {
    store.set(projObj.name, projObj);
    console.log('saved!');
};




//Run gulp task
exports.runTask = (taskName, projPath) => {
    const cp = spawn('gulp', [ taskName ], {cwd: projPath});
    cp.stdout.setEncoding('utf8');
    cp.stdout.on('data', data => {
        let outputArr = data.split(/(\r?\n)/g);
        mainWindow.webContents.send('task-console-data', outputArr);
    });

    if(taskPID === null) {
        setTimeout(function() {
            getPID('gulp');
        }, 5000, false);
    }

    cp.stderr.setEncoding('utf8');
    cp.stderr.on('data', data => {
        console.error(data);
        displayNotification({text: `[error] ${data}`});
        mainWindow.webContents.send('task-console-data', `[error] ${data}`);
    });

    cp.on('exit', code => {
        // checkForProcess('gulp');
        if (code === 0) {
          displayNotification({
            title: 'gulp',
            subtitle: 'Finished running tasks'
          });
        } else if ( !code || code === null ) {
            return;
        } else {
            console.error(`Exited with error code ${code}`);

            displayNotification({
                title: 'gulp',
                subtitle: `Exited with error code ${code}`,
                sound: 'Basso'
            });
        }
    });
};

//Watch for gulp task to end

const getPID = (name) => {
    process('name', 'gulp').then(list => {
        for(let i = 0; i < list.length; i++) {
            if(_.get(list[i], 'name') === 'gulp') {
                taskPID = _.get(list[i], 'pid');
                watchProcess(taskPID);
                break;
            } else {
                console.log('no pid');
                taskPID = null;
            }
        }
    }).catch(err => {
        console.log(err);
        if(_.get(err, 'errno') === 'EAGAIN' || _.get(err, 'code') === null) {
            return;
        }
    });

};

const watchProcess = (pid) => {
    exec(`ps -p ${pid}`, (error, stdout, stderr) => {
        if(error) {
            console.error(`exec error: ${error}`);
            taskPID = null;
            return;
        }

        let output = stdout.split('\n');
        output = output.splice(1, 1);

        console.log(`stdout: ${output[0]}`);

        if(output && output !== null && output !== undefined) {
            setTimeout(function() {
                watchProcess(taskPID);
            }, 5000, false);
        } else {
            taskPID = null;
            return;
        }
    })
};
