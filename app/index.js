/**
 * Created by mschroeter on 7/13/17.
 */
'use strict';
const electron = require('electron');
const ipcRenderer = electron.ipcRenderer;
const remote = electron.remote;
const main = remote.require('./main');
const runTask = main.runTask;
const saveNewProject = main.saveNewProject;
const fkill = require('fkill');
const Store = require('electron-store');

const dropArea = document.querySelector('.project-column');
const projectList = dropArea.querySelector('#project-list');
let taskListColumn = document.querySelector('#gulp-command-column');
let taskListList = document.querySelector('#gulp-command-list');
let itemMod = projectList.getElementsByClassName('item-mod');

const gulpRunningColumn= document.querySelector('#gulp-running-task');
const taskStatusBar = gulpRunningColumn.querySelector('#task-control');
const taskNameSpan = taskStatusBar.querySelector('#task-name');
const taskKillBtn = taskStatusBar.querySelector('#kill-task-button');
const gulpConsole = gulpRunningColumn.querySelector('#gulp-console');

let allProj = {};
let currentProj = {};
let currentTask = null;
const store = new Store;



//
//Core Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Add New Project to Store
const addNewProject = (item, path) => {
    let newProj = {};
    let hasGulp = false;

    const listTasks = (path) => {

        const parseGulpTasks = (taskList) => {
            newProj.taskList = taskList;
            saveNewProject(newProj);
            setCurrentProject(newProj, true);

            //Update allProj after store
            allProj = store.get();

            //Watch del button click event to delete item
            watchItemDel(document.getElementById(newProj.name).querySelector('.del-item'));
        };


        const getNewGulpTasks = (path) => {
            ipcRenderer.send('get-new-gulp-tasks', path);
            console.log('sent task');

            ipcRenderer.on('add-new-gulp-tasks', (event, arg) => {
                //Make sure we're getting a task list returned
                if(arg !== null && arg.constructor === Array) {
                    if(newProj.name && newProj.name !== null && newProj.name !== undefined) {
                        parseGulpTasks(arg);
                    }
                } else {
                    newProj = {};
                    arg = null;
                    hasGulp = false;
                    alert('Something is wrong with your gulpfile!');
                    return;
                }

            });
        };

        getNewGulpTasks(path);
    };


    const addProject = (item) => {
        newProj.name = item.name;
        let newItem = document.createElement('li');
        newItem.appendChild(document.createTextNode(item.name));
        projectList.appendChild(newItem);
    };

    path = path || "";
    if (item.isFile) {
        // Get file
        item.file(function(file) {
            alert(path + file.name + "is not a gulp project directory");
        });
    } else if (item.isDirectory) {
        // Get folder contents
        let dirReader = item.createReader();
        dirReader.readEntries(function(entries) {
            for (let i=0; i<entries.length; i++) {

                if ( entries[i].name === 'gulpfile.js' ) {
                    hasGulp = true;
                    newProj.path = path;
                    newProj.name = item.name;
                    listTasks(path);
                    break;
                } else {
                    hasGulp = false;
                }
            }

            if( !hasGulp ) {
                alert('Not a Gulp Project!');
                return;
            }
        });
    }
};




//Set Current Project
const setCurrentProject = (project, addToDOM) => {
  currentProj = project;

  if (addToDOM === true) {
    addProjectToDOM(project);
    setTaskList(project);
  } else {
    setTaskList(project);
  }

  let prevActiveProj = projectList.querySelectorAll('.current-project');
  if (prevActiveProj) {
    for (let i = 0; i < prevActiveProj.length; i++) {
      prevActiveProj[i].classList.remove('current-project');
    }
  }
  let activeProjectItem = projectList.querySelector(`#${currentProj.name}`);
  activeProjectItem.classList.add('current-project');

  if (addToDOM === true) {
    watchProject(activeProjectItem);
  }
};



//
//Helper Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Watch for clicks on task list
const watchTaskList = (taskItem) => {
  taskItem.addEventListener('click', function(e) {
    e.preventDefault();
    runTask(taskItem.innerText, currentProj.path);
    currentTask = taskItem.innerText;
    taskListColumn.classList.add('task-is-running');
    gulpRunningColumn.classList.add('task-is-running');
    taskNameSpan.innerText = currentTask;
  });
};




//Watch for clicks on project list
const watchProject = (projectItem) => {
  projectItem.addEventListener('click', function (e) {
    let projectName = projectItem.getAttribute('id');
    let newCurrentProject = allProj[projectName];
    console.log(projectName);
    e.preventDefault();
    setCurrentProject(newCurrentProject, false);
    console.log(`Current project: ${currentProj.name}`);
  })
};

const watchProjectList = (list) => {
  let projectItems = list.querySelectorAll('li');
  for( let i = 0; i < projectItems.length; i++ ) {
    watchProject(projectItems[i]);
  }
};




//Add multiple projects to DOM
const addProjectObjToDOM = (obj) => {
  for( let key in obj ) {
    addProjectToDOM(obj[key]);
  }
};

//Add single project to DOM
const addProjectToDOM = (obj) => {
  let newItem = document.createElement('li');
  let itemModDiv = document.createElement('div');
  let itemDel = document.createElement('a');

  //Create Link to Delete Item
  itemDel.setAttribute('href', '#');
  itemDel.setAttribute('class', 'del-item');
  itemDel.setAttribute('data-item', obj.name);
  itemDel.appendChild(document.createTextNode('X'));

  //Create Div for Item Modification Links
  itemModDiv.setAttribute('class', 'item-mod');
  itemModDiv.appendChild(itemDel);

  //Create li for item
  newItem.setAttribute('id', obj.name);
  newItem.appendChild(document.createTextNode(obj.name));
  newItem.appendChild(itemModDiv);

  projectList.appendChild(newItem);
};




//Set task list for current project
const setTaskList = (project) => {
    const taskList = project.taskList;

    //Remove any tasks from previous project
    while( taskListList.firstChild ){
        console.log('removed');
      taskListList.removeChild( taskListList.firstChild );
    }

    for( let i = 0; i < taskList.length; i++ ) {
      const newTask = document.createElement('li');
      newTask.appendChild(document.createTextNode(taskList[i]));
      taskListList.appendChild(newTask);
    }

    let taskItems = taskListList.querySelectorAll('li');
    for( let i = 0; i < taskItems.length; i++ ) {
      watchTaskList(taskItems[i]);
    }
};




//
//On Load
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

allProj = store.get();

if( Object.keys(allProj).length === 0 ) {
  console.log('empty store');
} else {
  currentProj = allProj[Object.keys(allProj)[0]];
  addProjectObjToDOM(allProj);
  setCurrentProject(currentProj);
  // setTaskList(currentProj);
}

watchProjectList(projectList);




//
//DOM Interaction
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Drag and drop project
document.ondragover = document.ondrop = (e) => {
    e.preventDefault();
};

dropArea.ondrop = (e) => {
    let dropPath = e.dataTransfer.files[0].path;
    // e.preventDefault();
    let items = event.dataTransfer.items;
    for (let i=0; i<items.length; i++) {
        // webkitGetAsEntry is where the magic happens
        let item = items[i].webkitGetAsEntry();
        if (item) {
            addNewProject(item, dropPath);
        }
    }
};




//Kill task with button
taskKillBtn.addEventListener('click', (e) => {
    e.preventDefault();
    fkill('gulp');
    taskListColumn.classList.remove('task-is-running');
    gulpRunningColumn.classList.remove('task-is-running');
    taskNameSpan.innerText = "";
});




//Show console on gulp console data
ipcRenderer.on('task-console-data', (event, arg) => {
    console.log(arg);
    for( let i = 0; i < arg.length; i++ ) {
        // let outputStr = arg[i];
        let consoleData = document.createElement('li');
        consoleData.appendChild(document.createTextNode(arg[i]));
        gulpConsole.appendChild(consoleData);
    }

    //Auto scroll console readout
    window.setInterval(function() {
        gulpConsole.scrollTop = gulpConsole.scrollHeight;
    }, 5000);

});



//Item Mod Functionality
const watchItemDel = (modDel) => {
    modDel.addEventListener('click', (e) => {
        e.preventDefault();
        let modItem = modDel.getAttribute('data-item');
        modDel.modItem = modItem;
        let projectItem = document.getElementById(modDel.modItem);
        projectItem.parentNode.removeChild(projectItem);
        store.delete(modDel.modItem);
        allProj = store.get();
        location.reload(true);
    });
};

for( let i = 0; i < itemMod.length; i++ ) {
    let modDel = itemMod[i].querySelector('.del-item');
    watchItemDel(modDel);
}

